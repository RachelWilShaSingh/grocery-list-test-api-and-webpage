# https://realpython.com/api-integration-in-python/#get

import requests

url = "http://rachel.likespizza.com/grocerytest/index.php/grocery"

print( "url:", url )

response = requests.get( url )

print( "response", response )
print( "response.status_code", response.status_code )
print( "response.headers", response.headers ) 
print( "response.content", response.content ) 
print( "response.json()", response.json() ) 
