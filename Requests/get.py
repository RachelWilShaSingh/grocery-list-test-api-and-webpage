# https://realpython.com/api-integration-in-python/#get

import requests

id = int( input( "Enter ID: " ) )

url = "http://rachel.likespizza.com/grocerytest/index.php/grocery/" + str( id )

print( "url:", url )

response = requests.get( url )

print( "response", response )
print( "response.status_code", response.status_code )
print( "response.headers", response.headers ) 
print( "response.content", response.content ) 
print( "response.json()", response.json() ) 
