<?
class Controller
{
  private $groceryList;
  private $dataPath;
  private $requestMethod;
  private $id = null;
  
  public function __construct( $requestMethod, $id )
  {
    $this->path = "data/list.json"; 
    $this->requestMethod = $requestMethod;
    $this->id = $id;
  }
  
  public function request()
  {
    $response = array();
    $response["status_code_header"] = "HTTP/1.1 200 OK";
    $response["body"] = null;
    
    if ( $this->requestMethod == "GET" )
    {
      if ( $this->id == null )
      {
        $response["body"] = json_encode( [ [ "id" => "1", "name" => "eggs", "quantity" => "2" ], [ "id" => "2", "name" => "milk", "quantity" => "1" ] ] );
      }
      else
      {
        $response["body"] = json_encode( [ [ "id" => "1", "name" => "eggs", "quantity" => "2" ] ] );
      }
    }
    else if ( $this->requestMethod == "POST" )
    {
    }
    else if ( $this->requestMethod == "PUT" )
    {
    }
    else if ( $this->requestMethod == "DELETE" )
    {
    }
    else
    {
      $response["status_code_header"] = "HTTP/1.1 404 Not Found";
    }
    
    if ( $response["body"] != null )
    {
      echo ( $response["body"] );
    }
    else
    {
      echo( $response["status_code_header"] );
    }
  }
  
};
?>
