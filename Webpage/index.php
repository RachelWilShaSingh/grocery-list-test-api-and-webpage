<?
/*
 * Referencing: https://developer.okta.com/blog/2019/03/08/simple-rest-api-php
 * 
 * GET    /grocery          Retrieve all grocery items
 * GET    /grocery/{id}     Retrieve one grocery item
 * POST   /grocery          Create a new grocery item
 * PUT    /grocery/{id}     Update an existing grocery item
 * DELETE /grocery/{id}     Delete a grocery item
 * */

require "Controller.php";
 
$uri = parse_url( $_SERVER["REQUEST_URI"], PHP_URL_PATH );
$parts = explode( "/", $uri );
$idPos = 4;
$id = null;
if ( isset( $parts[$idPos] ) )
{
  $id = (int)$parts[$idPos];
}

$requestMethod = $_SERVER["REQUEST_METHOD"];

$controller = new Controller( $requestMethod, $id );
$controller->request();

//header( $response["status_code_header"] );

?>
