<?

class GroceryItem
{
  private $name = "";
  private $quantity = 0;
  private $id = 0;
  
  public function __construct()
  {
  }
  
  public function Setup( $id, $name, $quantity )
  {
    $this->SetId( $id );
    $this->SetName( $name );
    $this->SetQuantity( $quantity );
  }
  
  public function SetId( $id )
  {
    $this->id = $id;
  }
  
  public function SetName( $name )
  {
    $this->name = $name;
  }
  
  public function SetQuantity( $quantity )
  {
    $this->quantity = $quantity;
  }
  
  public function GetName()
  {
    return $this->name;
  }
  
  public function GetQuantity()
  {
    return $this->quantity;
  }
}

?>
